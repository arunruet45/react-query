import {useState} from 'react'
import './App.css'
import AppRoutes from "./AppRoutes";
import {BrowserRouter, Link} from "react-router-dom";
import routes from "./constants/Routes";
import {QueryClientProvider, QueryClient} from "react-query";
import {ReactQueryDevtools} from "react-query/devtools";

const queryClient = new QueryClient();

function App() {
    return (
        <QueryClientProvider client={queryClient}>
            <BrowserRouter>
                <div className="bg-primary">
                    <div className="d-flex bg-primary align-items-center m-auto justify-content-between text-dark"
                         style={{height: 50}}>
                        <Link to={routes.home} className="w-50 text-center text-white border-right">React Query</Link>
                        <Link to={routes.rtkQueries} className="w-50 text-center text-white">Rtk Query</Link>
                    </div>
                </div>
                {AppRoutes}
            </BrowserRouter>
            <ReactQueryDevtools initialIsOpen={false} position="bottom-right"/>
        </QueryClientProvider>
    )
}

export default App
