import {useParams} from "react-router-dom";
import {useGetStudentsDataById} from "../hooks/react-query-hooks/useGetStudentsById";

const StudentDetails = () => {
    const {id} = useParams();
    const {isLoading, data: student, isFetching, isError, error,} = useGetStudentsDataById(parseInt(id));
    console.log('iddd ',{isLoading, isFetching})
    return (
        <div className="text-center">
            <h3>React Query Students</h3>
            {isLoading && (
                <div className="spinner-border text-primary" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            )}
            {isError && (
                <h3>{error.message}</h3>
            )}
            {!isLoading && Object.keys(student?.data).length > 0 && (
                <h5>Name: {student?.data?.name}, Hobby: {student?.data?.hobby}, details: {student?.data?.details}
                </h5>
            )}
        </div>
    )
};

export default StudentDetails;