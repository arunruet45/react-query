import React from "react";
import {Link} from "react-router-dom";
import routes from "../../constants/Routes";

const RQHome = () => {
    return (
        <div className="text-center mt-2">
            <Link to={routes.normalStudents} className="btn btn-primary mr-2">Normal query</Link>
            <Link to={routes.rqStudents} className="btn btn-primary mr-2">React query</Link>
            <Link to={routes.parallelQueries} className="btn btn-primary mr-2">Parallel Queries</Link>
            <Link to={routes.dynamicParallelQueries} className="btn btn-primary mr-2">Dynamic Parallel Queries</Link>
            <Link to={routes.dependentQueries} className="btn btn-primary mr-2">Dependent queries</Link>
            <Link to={routes.addStudent} className="btn btn-primary mr-2">Add student</Link>
        </div>
    )
};

export default RQHome;