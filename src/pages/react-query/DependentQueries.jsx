import axios from "axios";
import {useQuery} from "react-query";

const fetchStudents = async () => {
    return await axios.get('http://localhost:4000/students');
};

const fetchTeacherById = async ({queryKey}) => {
    const id = queryKey[1];
    return await axios.get(`http://localhost:4000/teachers/${id}`);
};

const DependentQueries = () => {
    const {data: students} = useQuery('get-students', fetchStudents);
    const id = students?.data[0].id
    const {data: teachers} = useQuery(['get-teachers-by-id', id], fetchTeacherById, {enabled: id === 1});
    // console.log({students, teachers});
    return (
        <div className="text-center">
            <h3>Dependent Queries</h3>
        </div>
    )
};

export default DependentQueries;