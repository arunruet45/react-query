import {useState} from "react";
import {useAddStudent} from "../../hooks/react-query-hooks/useAddStudent";
import RQStudents from "./RQStudents";

const AddStudent = () => {
    const [name, setName] = useState('');
    const [hobby, setHobby] = useState('');

    const {mutate: addStudent, isSuccess} = useAddStudent();

    const onClickAddStudent = () => {
        const student = {name, hobby}
        addStudent(student);
    };

    return (
        <div className="text-center">
            <h3>Add Student</h3>
            Name: <input type="text" className="mr-2" onChange={(e) => setName(e.target.value)}/>
            Hobby: <input type="text" className="mr-2" onChange={(e) => setHobby(e.target.value)}/>
            <button onClick={onClickAddStudent} className="btn btn-primary">Add</button>
            {isSuccess && (
                <h5 className="text-success">Done</h5>
            )}

            {/*// comment bellow line to show query invalid topic*/}
            <RQStudents/>
        </div>
    )

};

export default AddStudent;