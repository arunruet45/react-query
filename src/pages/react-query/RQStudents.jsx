import React from "react";
import {useQuery} from "react-query";
import axios from "axios";
import {Link} from "react-router-dom";
import routes from "../../constants/Routes";
import {useGetStudentsData} from "../../hooks/react-query-hooks/useGetStudentsData";

const fetchStudents = async () => {
    return await axios.get('http://localhost:4000/students');
};

const RQStudents = () => {
    const onSuccessGetStudents = (data) => {
        console.log('Get Student Success :', data);
    }

    const onerrorGetStudent = (error) => {
        console.log('Get Student Error :', error);
    };

    const dataTransformation = (students) => {
        return students?.data.map((student) => {
            return {
                id: student.id,
                name: student.name,
                hobby: student.hobby
            }
        })
    }


    const {isLoading, data: students, isError, error, isSuccess, isFetching, refetch: getStudents} = useQuery(
        'get-students',
        fetchStudents,
        {
            cacheTime: 5 * 1000 * 60, // number, infinity
            staleTime: 20*1000, // number, infinity
            refetchOnMount: true, // boolean, 'always'
            refetchOnWindowFocus: false, // boolean, 'always'
            refetchInterval: false, // number, boolean, func
            refetchIntervalInBackground: false, // boolean
            onSuccess: onSuccessGetStudents,
            onError: onerrorGetStudent,
            // select: dataTransformation,
        }
    );

    // const {isLoading, data: students, isError, error, isFetching, refetch: getStudents} = useGetStudentsData(onSuccessGetStudents,
    //     onerrorGetStudent, dataTransformation);

    console.log({isLoading, isFetching});

    return (
        <div className="text-center">
            <h3>React Query Students</h3>
            {isLoading && (
                <div className="spinner-border text-primary" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            )}
            {isError && (
                <h3>{error.message}</h3>
            )}
            {!isLoading && students?.data?.length > 0 && (
                <>
                    <button type="button" className="btn btn-primary" onClick={getStudents}>Get Student</button>
                    {students?.data?.map((student, index) => {
                        return (
                            <h5 key={index}>
                                Name: {student?.name}, Hobby: {student?.hobby},
                                <Link to={`${routes.rqStudents}/${student.id}`}>Details</Link>
                            </h5>
                        )
                    })}
                </>
            )}

            {/*when use Select option query uncomment bellow code*/}

            {/*{!isLoading && students?.length > 0 && (*/}
            {/*    <>*/}
            {/*        <button type="button" className="btn btn-primary" onClick={getStudents}>Get Student</button>*/}
            {/*        {students.map((student, index) => {*/}
            {/*            return (*/}
            {/*                <h5 key={index}>*/}
            {/*                    Name: {student?.name}, Hobby: {student?.hobby},*/}
            {/*                    <Link to={`${routes.rqStudents}/${student.id}`}>Details</Link>*/}
            {/*                </h5>*/}
            {/*            )*/}
            {/*        })}*/}
            {/*    </>*/}
            {/*)}*/}
        </div>
    )
};

export default RQStudents;