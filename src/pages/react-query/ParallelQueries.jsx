import axios from "axios";
import {useQuery} from "react-query";

const fetchStudents = async () => {
    return await axios.get('http://localhost:4000/students');
};

const fetchTeachers = async () => {
    return await axios.get('http://localhost:4000/teachers');
};

const ParallelQueries = () => {
    const {data: students} = useQuery('get-students', fetchStudents);
    const {data: teachers} = useQuery('get-teachers', fetchTeachers);
    // console.log({students, teachers})
    return (
        <div className="text-center">
            <h3>Parallel Queries</h3>
        </div>
    )
};

export default ParallelQueries;