import React, {useEffect, useState} from "react";
import axios from "axios";
import RQStudents from "./RQStudents";

const NormalStudents = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [students, setStudents] = useState([]);
    const [error, setError] = useState('')

    useEffect(() => {
        const getData = async () => {
            setIsLoading(true);
            await Promise.allSettled([await getStudents()]);
            setIsLoading(false);
        };

        getData();

    }, []);

    const getStudents = async () => {
        try {
            const students = await axios.get('http://localhost:4000/students');
            setStudents(students.data);
        } catch (e) {
            console.log(e);
            setError(e.message);
        }
    };

    return (
        <div className="text-center">
            <h3>Normal Students</h3>
            {isLoading && (
                <div className="spinner-border text-primary" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            )}
            {error && (
                <h3>{error}</h3>
            )}
            {!isLoading && students.length > 0 && (
                <>
                    {students.map((student, index) => {
                        return (
                            <h3 key={index}>
                                Name: {student.name},  Hobby: {student.hobby}
                            </h3>
                        )
                    })}
                </>
            )}

            {/*for understanding deduping*/}
            {/*<RQStudents/>*/}
            {/*<RQStudents/>*/}
        </div>
    )
};

export default NormalStudents;

