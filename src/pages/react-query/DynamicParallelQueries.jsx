import axios from "axios";
import {useQueries, useQuery} from "react-query";

const fetchStudentById = async ({queryKey}) => {
    const id = queryKey[1];
    return await axios.get(`http://localhost:4000/students/${id}`);
};

const DynamicParallelQueries = ({ids}) => {
    const queryResults = useQueries(
        ids.map((id)=> {
            return {
                queryKey: ['get-dynamic-parallel-students-by-id', id],
                queryFn: fetchStudentById
            }
        })
    )
    // console.log({queryResults})
    return (
        <div className="text-center">
            <h3>Dynamic Parallel Queries</h3>
        </div>
    )
};

export default DynamicParallelQueries;