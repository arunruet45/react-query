import React from "react";
import {Link} from "react-router-dom";
import routes from "../../constants/Routes";
import {useGetTeachersQuery} from "../../redux/api";
import {Provider} from "react-redux";
import {store} from "../../redux/store";

const Home = () => {
    const {data: teachers, refetch: getTeachers} = useGetTeachersQuery('getUser');

    return (
        <div className="text-center mt-2">
            <Link to={routes.rtkGetQueries} className="btn btn-primary mr-2">Get Queries</Link>
            <Link to={routes.rtkMutation} className="btn btn-primary mr-2">Mutate Queries</Link>
        </div>
    )
};

const RTKHome = () => {
    return (
        <Provider store={store}>
            <Home/>
        </Provider>
    )
}

export default RTKHome