import React from "react";
import {useQuery} from "react-query";
import axios from "axios";
import {Link} from "react-router-dom";
import routes from "../../constants/Routes";
import {useGetStudentsData} from "../../hooks/react-query-hooks/useGetStudentsData";
import {Provider} from "react-redux";
import {store} from "../../redux/store";
import {useGetStudentsQuery, useGetTeachersQuery} from "../../redux/api"

const fetchStudents = async () => {
    return await axios.get('http://localhost:4000/students');
};

const RTKStudents = () => {
    const onSuccessGetStudents = (data) => {
        console.log('Get Student Success :', data);
    }

    const onerrorGetStudent = (error) => {
        console.log('Get Student Error :', error);
    };

    const dataTransformation = (students) => {
        return students?.data.map((student) => {
            return {
                id: student.id, name: student.name, hobby: student.hobby
            }
        })
    }

    const {
        data: students, error, isLoading, isFetching, isError, refetch: getStudents
    } = useGetStudentsQuery(1, {
        skip: false,
        pollingInterval: 0,
        refetchOnMountOrArgChange: true,
        refetchOnFocus: true,
        refetchOnReconnect: true,
        ifOlderThan: 5
        // invalidatesTags: ["Teachers"]
    });
    console.log({students})

    const {data: teachers, refetch: getTeachers} = useGetTeachersQuery();
    console.log({teachers})

    return (<div className="text-center">
        <h3>RTK Query Students</h3>
        {isLoading && (<div className="spinner-border text-primary" role="status">
            <span className="sr-only">Loading...</span>
        </div>)}
        {isError && (<h3>{error.message}</h3>)}
        {!isLoading && students?.length > 0 && (<>
            <button type="button" className="btn btn-primary mr-2" onClick={getStudents}>Get Student</button>
            <button type="button" className="btn btn-primary" onClick={getTeachers}>Get Teachers</button>
            {students?.map((student, index) => {
                return (
                    <h5 key={index}>
                        Name: {student?.name}, Hobby: {student?.hobby},
                        <Link className="ml-2" to={`${routes.rtkStudents}/${student.id}`}>Details</Link>
                    </h5>
                )
            })}
        </>)}
    </div>)
};

const RTKGetQueries = () => {
    return (
        <Provider store={store}>
            <RTKStudents/>
        </Provider>
    )
}

export default RTKGetQueries