import React, {useState} from "react";
import {Provider} from "react-redux";
import {store} from "../../redux/store";
import {useAddStudentMutation, useGetStudentsQuery} from "../../redux/api";
import RTKGetQueries from "./RTKGetQueries";

const AddStudent = () => {
    const [name, setName] = useState('');
    const [hobby, setHobby] = useState('');


    const [addStudent, isSuccess] = useAddStudentMutation();

    console.log(isSuccess)

    const onClickRTKAddStudent = () => {
        const student = {name, hobby}
        addStudent(student);
    };

    return (
        <div className="text-center">
            <h3>Add Student</h3>
            Name: <input type="text" className="mr-2" onChange={(e) => setName(e.target.value)}/>
            Hobby: <input type="text" className="mr-2" onChange={(e) => setHobby(e.target.value)}/>
            <button onClick={onClickRTKAddStudent} className="btn btn-primary">RTK Add</button>
            <RTKGetQueries/>
        </div>
    )

};

const RTKMutation = () => {
    return (
        <Provider store={store}>
            <AddStudent/>
        </Provider>
    )
}

export default RTKMutation