import {useParams} from "react-router-dom";
import {Provider} from "react-redux";
import {store} from "../../redux/store";
import React from "react";
import {useGetStudentByIdQuery} from "../../redux/api";

const StudentDetails = () => {
    const {id} = useParams();
    const {isLoading, data: student, isFetching, isError, error,} = useGetStudentByIdQuery(parseInt(id));

    return (
        <div className="text-center">
            <h3>React Query Students</h3>
            {isLoading && (
                <div className="spinner-border text-primary" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            )}
            {isError && (
                <h3>{error.message}</h3>
            )}
            {!isLoading && Object.keys(student).length > 0 && (
                <h5>Name: {student.name}, Hobby: {student.hobby},  details: {student.details}
                </h5>
            )}
        </div>
    )
};


const RTKStudentDetails = () => {
    return (
        <Provider store={store}>
            <StudentDetails/>
        </Provider>
    )
}

export default RTKStudentDetails