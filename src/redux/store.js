import {configureStore, getDefaultMiddleware} from "@reduxjs/toolkit";
import {teacherStudentApi} from "./api";

export const store = configureStore({
    reducer: {
        [teacherStudentApi.reducerPath]: teacherStudentApi.reducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(teacherStudentApi.middleware)
})