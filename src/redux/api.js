import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";

export const teacherStudentApi = createApi({
    reducerPath: "teacherStudentApi",
    tagTypes: ["Students", "Teachers"],
    keepUnusedDataFor: 10,
    refetchOnMountOrArgChange: true,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    baseQuery: fetchBaseQuery({baseUrl: "http://localhost:4000/"}),
    endpoints: (builder) => ({
        getStudents: builder.query({
            query: () => 'students',
            keepUnusedDataFor: 5,
            providesTags: ["Students"],
            invalidatesTags: ["Teachers"]
            // transformResponse: (data, meta, arg) => response.data,
        }),
        getStudentById: builder.query({
            query: (id) => `students/${id}`,
        }),
        getTeachers: builder.query({
            query: () => `teachers`,
            providesTags: ["Teachers"],
            refetchOnMountOrArgChange: true,
            keepUnusedDataFor: 5
        }),
        addStudent: builder.mutation({
            query: (body) => ({
                url: 'students',
                method: 'POST',
                body,
            }),
            invalidatesTags: ['Students'],
        }),
    })
})

export const {
    useGetStudentsQuery,
    useGetTeachersQuery,
    useAddStudentMutation,
    useGetStudentByIdQuery
} = teacherStudentApi;