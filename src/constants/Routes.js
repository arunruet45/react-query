export default {
    home: "/",
    normalStudents: "/normal-students",
    rqStudents: "/rq-students",
    parallelQueries: "/parallel-queries",
    dynamicParallelQueries: "/dynamic-parallel-queries",
    dependentQueries: "/dependent-queries",
    addStudent: "/add-student",
    rtkQueries: "/rtk-query-queries",
    rtkGetQueries: "/rtk-query-get-queries",
    rtkMutation: "/rtk-query-mutation",
    rtkStudents: "/rtk-query-students",
};