import React from "react";
import {Route, Routes} from "react-router-dom";
import NormalStudents from "./pages/react-query/NormalStudents";
import RQStudents from "./pages/react-query/RQStudents";
import RQHome from "./pages/react-query";
import routes from "./constants/Routes";
import StudentDetails from "./pages/StudentDetails";
import ParallelQueries from "./pages/react-query/ParallelQueries";
import DynamicParallelQueries from "./pages/react-query/DynamicParallelQueries";
import DependentQueries from "./pages/react-query/DependentQueries";
import AddStudent from "./pages/react-query/AddStudent";
import RTKHome from "./pages/rtk-query";
import RTKGetQueries from "./pages/rtk-query/RTKGetQueries";
import RTKMutation from "./pages/rtk-query/RTKMutation";
import RTKStudentDetails from "./pages/rtk-query/StudentDetails";

export default (
        <Routes>
            <Route exact path={routes.home} element={<RQHome/>}/>
            <Route exact path={routes.normalStudents} element={<NormalStudents/>}/>
            <Route exact path={routes.rqStudents} element={<RQStudents/>}/>
            <Route exact path={`${routes.rqStudents}/:id`} element={<StudentDetails/>}/>
            <Route exact path={routes.parallelQueries} element={<ParallelQueries/>}/>
            <Route exact path={routes.dynamicParallelQueries} element={<DynamicParallelQueries ids={[1,2]}/>}/>
            <Route exact path={routes.dependentQueries} element={<DependentQueries/>}/>
            <Route exact path={routes.addStudent} element={<AddStudent/>}/>
            <Route exact path={routes.rtkQueries} element={<RTKHome/>}/>
            <Route exact path={routes.rtkGetQueries} element={<RTKGetQueries/>}/>
            <Route exact path={routes.rtkMutation} element={<RTKMutation/>}/>
            <Route exact path={`${routes.rtkStudents}/:id`} element={<RTKStudentDetails/>}/>
        </Routes>
);