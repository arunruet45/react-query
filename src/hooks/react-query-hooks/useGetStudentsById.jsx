import {useQuery, useQueryClient} from "react-query";
import axios from "axios";

const fetchStudentById = async ({queryKey}) => {
    const id = queryKey[1];
    return await axios.get(`http://localhost:4000/students/${id}`);
};

export const useGetStudentsDataById = (id) => {
    const queryClient = useQueryClient();
    return useQuery(
        ['get-student-by-id', id],
        fetchStudentById,
        {
            onSuccess: async () => {
                await queryClient.invalidateQueries('get-students');
            },
            cacheTime: 5*60*1000, // number, infinity
            staleTime: 0, // number, infinity
            // refetchOnMount: true, // boolean, 'always'
            // refetchOnWindowFocus: false, // boolean, 'always'
            // refetchInterval: false, // number, boolean, func
            // refetchIntervalInBackground: true, // boolean
            initialData: () => {
                const student = queryClient.getQueryData('get-students')?.data?.find((student) => student.id === id);
                return student ? {data: student} : undefined;
            }
        }
    );
};