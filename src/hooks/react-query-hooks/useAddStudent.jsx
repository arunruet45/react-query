import axios from "axios";
import {useMutation, useQueryClient} from "react-query";

const addStudent = (student) => {
    return axios.post('http://localhost:4000/students', student);
}

export const useAddStudent = () => {
    const queryClient = useQueryClient();
    return useMutation(addStudent,

        {
            // query invalidation
            // onSuccess: async () => {
            //     console.log('success')
            //     await queryClient.invalidateQueries('get-students');
            // },
            // update list using form data
            onSuccess: (data) => {
                queryClient.setQueryData('get-students', (prevData) => {
                    return {
                        data: [...prevData.data, data.data]
                    }
                });
            },
            onMutate: () => {
                console.log('mutaiton')
            }, // called before mutation function
            onError: () => {
            }, // for error
            onSettled: () => {
            } // either success or error
        }
    );
};
