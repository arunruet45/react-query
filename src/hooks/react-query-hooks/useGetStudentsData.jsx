import {useQuery} from "react-query";
import axios from "axios";

const fetchStudents = async () => {
    return await axios.get('http://localhost:4000/students');
}

export const useGetStudentsData = (onSuccessGetStudents, onerrorGetStudent, dataTransformation) => {
    return  useQuery(
        'get-hook-students',
        fetchStudents,
        {
            cacheTime: 50000, // number, infinity
            staleTime: 0, // number, infinity
            refetchOnMount: true, // boolean, 'always'
            refetchOnWindowFocus: false, // boolean, 'always'
            refetchInterval: false, // number, boolean, func
            refetchIntervalInBackground: true, // boolean
            onSuccess: onSuccessGetStudents,
            onError: onerrorGetStudent,
            select: dataTransformation,
        }
    );
};